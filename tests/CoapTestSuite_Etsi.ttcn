///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               CoapTestSuite_Etsi.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 910
//  Updated:            2021-02-03
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module CoapTestSuite_Etsi
{
	import from IoT_FT_Framework_Definitions all;
	import from IoT_FT_Framework_Functions all;
	import from IFW_CoAP_Peer_TestSteps all;
	import from CoAP_Types all;

	testcase tc_client_TD_COAP_CORE_01() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_01_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_01_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_01_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := CONFIRMABLE,
    		code := METHOD_GET,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		}
		},
    	payload := omit
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_01_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := ACKNOWLEDGEMENT,
	        code := RESPONSE_CODE_Content,
	        message_id := ?
	    },
	    token := ''O,
	    options := 
	    {
	 		{
	    		content_format := 0
	        }
	    },
    	payload := ?
	}
	
	testcase tc_client_TD_COAP_CORE_02() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_02_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_02_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_02_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := CONFIRMABLE,
    		code := METHOD_POST,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		},
	  		{
	    		content_format := 0
	        }
		},
    	payload := char2oct("Some payload")
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_02_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := ACKNOWLEDGEMENT,
	        code := RESPONSE_CODE_Created,
	        message_id := ?
	    },
	    token := ''O,
	    options := 
	    {
	    },
    	payload := omit
	}
	
	testcase tc_client_TD_COAP_CORE_03() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_03_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_03_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_03_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := CONFIRMABLE,
    		code := METHOD_PUT,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		},
	  		{
	    		content_format := 0
	        }
		},
    	payload := char2oct("Some payload")
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_03_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := ACKNOWLEDGEMENT,
	        code := RESPONSE_CODE_Changed,
	        message_id := ?
	    },
	    token := ''O,
	    options := 
	    {
	    },
    	payload := omit
	}
	
	testcase tc_client_TD_COAP_CORE_04() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_04_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_04_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_04_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := CONFIRMABLE,
    		code := METHOD_DELETE,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		}
		},
    	payload := omit
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_04_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := ACKNOWLEDGEMENT,
	        code := RESPONSE_CODE_Deleted,
	        message_id := ?
	    },
	    token := ''O,
	    options := omit,
    	payload := omit
	}
	
	testcase tc_client_TD_COAP_CORE_05() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_05_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_05_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_05_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := NON_CONFIRMABLE,
    		code := METHOD_GET,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		}
		},
    	payload := omit
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_05_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := NON_CONFIRMABLE,
	        code := RESPONSE_CODE_Content,
	        message_id := ?
	    },
	    token := ''O,
	    options := 
	    {
	 		{
	    		content_format := 0
	        }
	    },
    	payload := ?
	}
	
	testcase tc_client_TD_COAP_CORE_06() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_06_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_06_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_06_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := NON_CONFIRMABLE,
    		code := METHOD_POST,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		},
	  		{
	    		content_format := 0
	        }
		},
    	payload := char2oct("Some payload")
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_06_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := NON_CONFIRMABLE,
	        code := RESPONSE_CODE_Created,
	        message_id := ?
	    },
	    token := ''O,
	    options := 
	    {
	    },
    	payload := omit
	}
	
	testcase tc_client_TD_COAP_CORE_07() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_07_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_07_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_07_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := NON_CONFIRMABLE,
    		code := METHOD_PUT,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		},
	  		{
	    		content_format := 0
	        }
		},
    	payload := char2oct("Some payload")
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_07_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := NON_CONFIRMABLE,
	        code := RESPONSE_CODE_Changed,
	        message_id := ?
	    },
	    token := ''O,
	    options := 
	    {
	    },
    	payload := omit
	}
	
	testcase tc_client_TD_COAP_CORE_08() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_CORE_08_req00));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_CORE_08_rsp01))
	  {
	  	setverdict(fail);
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_08_req00 :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := NON_CONFIRMABLE,
    		code := METHOD_DELETE,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "test"
	  		}
		},
    	payload := omit
	}
	
	template CoAP_ReqResp t_client_TD_COAP_CORE_08_rsp01 :=
	{
		header := 
		{
	        version := 1,
	        msg_type := NON_CONFIRMABLE,
	        code := RESPONSE_CODE_Deleted,
	        message_id := ?
	    },
	    token := ''O,
	    options := omit,
    	payload := omit
	}	
	
	testcase tc_client_TD_COAP_BLOCK_01() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  var boolean getNext := true;
	  var integer num := 0;
	  
	  while (getNext) 
	  {
		f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_BLOCK_01_req00(num)));
		f_IFW_CoapPeer_send(client);
	  
		f_IFW_CoapPeer_receive(client);
		
		// Check the expected answer
		if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_BLOCK_01_rsp01(num, ?))) {
		  getNext := false;
		  setverdict(fail);
		}
		
		// Check if it is the last
		if (f_IFW_CoapPeer_check(client, t_client_TD_COAP_BLOCK_01_rsp01(num, false))) {
		  getNext := false;
		}
				
		num := num + 1;
	  }
	}
	
	template CoAP_ReqResp t_client_TD_COAP_BLOCK_01_req00(integer p_num) :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := CONFIRMABLE,
    		code := METHOD_GET,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "large"
	  		},
	  		{
	    	  block2 := {
	    	    num := p_num,
	    	    m := false,
	    	    szx := 2
	    	  }
	        }
		},
    	payload := omit
	}	
	
	template CoAP_ReqResp t_client_TD_COAP_BLOCK_01_rsp01(template integer p_num, template boolean p_m) :=
	{
		header := 
		{
	        version := 1,
	        msg_type := ACKNOWLEDGEMENT,
	        code := RESPONSE_CODE_Content,
	        message_id := ?
	    },
	    token := ''O,
	    options := {
	  		{
	    	  block2 := {
	    	    num := p_num,
	    	    m := p_m,
	    	    szx := 2
	    	  }
	        }
	    },
    	payload := ?
	}
	
	
	
	testcase tc_client_TD_COAP_OBS_01() runs on IFW_MAIN_CT
	{
	  setverdict(pass);
	  
	  var integer client := f_IFW_addComponent(COAP_PEER, "client");
	  
	  f_IFW_CoapPeer_setRemote(client, "server");
	  
	  f_IFW_initComponents();
	  
	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_OBS_01_register));
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_OBS_01_registered))
	  {
	  	setverdict(fail);
	  }
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_OBS_01_notification, DO_NOT_CHECK_MID))
	  {
	  	setverdict(fail);
	  }
	  
 	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_OBS_01_acknowledgement), USE_LAST_RECEIVED_MID);
	  f_IFW_CoapPeer_send(client);
	  
	  f_IFW_CoapPeer_receive(client);
	  if (not f_IFW_CoapPeer_check(client, t_client_TD_COAP_OBS_01_notification, DO_NOT_CHECK_MID))
	  {
	  	setverdict(fail);
	  }
	  
 	  f_IFW_CoapPeer_setMessageToSend(client, valueof(t_client_TD_COAP_OBS_01_reset), USE_LAST_RECEIVED_MID);
	  f_IFW_CoapPeer_send(client);
	}
	
	template CoAP_ReqResp t_client_TD_COAP_OBS_01_register :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := CONFIRMABLE,
    		code := METHOD_GET,
    		message_id := 0
	  	},
    	token := ''O,
    	options := 
		{
	  		{
	  		  uri_path := "obs"
	  		},
	  		{
	  		  observe := 0
	  		}
		},
    	payload := omit
	}
	
	template CoAP_ReqResp t_client_TD_COAP_OBS_01_registered :=
	{
		header := 
		{
	        version := 1,
	        msg_type := ACKNOWLEDGEMENT,
	        code := RESPONSE_CODE_Content,
	        message_id := ?
	    },
	    token := ''O,
	    options := {
	      {
	        observe := ?
	      }
	    },
    	payload := ?
	}
	
	template CoAP_ReqResp t_client_TD_COAP_OBS_01_notification :=
	{
		header := 
		{
	        version := 1,
	        msg_type := CONFIRMABLE,
	        code := RESPONSE_CODE_Content,
	        message_id := ?
	    },
	    token := ''O,
	    options := {
	      {
	        observe := ?
	      }
	    },
    	payload := ?
	}	
	
	template CoAP_ReqResp t_client_TD_COAP_OBS_01_acknowledgement :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := ACKNOWLEDGEMENT,
    		code := EMPTY_MESSAGE,
    		message_id := 0
	  	},
    	token := ''O,
    	options := omit,
    	payload := omit
	}
	
	template CoAP_ReqResp t_client_TD_COAP_OBS_01_reset :=
	{
	  	header := 
	  	{
    		version := 1,
    		msg_type := RESET,
    		code := EMPTY_MESSAGE,
    		message_id := 0
	  	},
    	token := ''O,
    	options := omit,
    	payload := omit
	}
		
	control
	{
	  execute(tc_client_TD_COAP_CORE_01());
	  execute(tc_client_TD_COAP_CORE_02());
	  execute(tc_client_TD_COAP_CORE_03());
	  execute(tc_client_TD_COAP_CORE_04());
	  execute(tc_client_TD_COAP_CORE_05());
	  execute(tc_client_TD_COAP_CORE_06());
	  execute(tc_client_TD_COAP_CORE_07());
	  execute(tc_client_TD_COAP_CORE_08());
	  execute(tc_client_TD_COAP_BLOCK_01());
	  execute(tc_client_TD_COAP_OBS_01());
	}
	
}
