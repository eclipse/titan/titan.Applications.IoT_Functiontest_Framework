///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
///////////////////////////////////////////////////////////////////////////////
//  File:               IFW_Common.ttcn
//  Description:
//  Rev:                R1B
//  Prodnr:             CNL 113 910
//  Updated:            2021-02-03
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
//  Module: IFW_Common
// 
//  Purpose:
//    This module contains common definitions for the IoT Function Test Framework
///////////////////////////////////////////////////////////////
module IFW_Common {

	import from IPL4asp_Types all;
	
    ///////////////////////////////////////////////////////////
    //  Module parameter: tsp_addresses
    // 
    //  Purpose:
    //    Module parameters for storing network addresses
    //
    //  Type:
    //     <Address_List>
    //
    //  Default value:
    //     empty list
    //
    //  Example:
    //    (start code)
    //    tsp_addresses :=
    //    {
    //      {
    //        id := "enb",
    //        hostName := "10.35.85.194",
    //        portNumber := 36422
    //      },
    //      {
    //        id := "mme",
    //        hostName := "10.35.84.240",
    //        portNumber := 36412
    //      }
    //    }
    //    (end)
    //
    ///////////////////////////////////////////////////////////
	modulepar
	{
	  Address_List tsp_addresses := {};
	}
	
	type record of integer IntegerList;
	
	///////////////////////////////////////////////////////////
    //  Type: Address_List
    //
    //  Purpose:
    //    List of <NamedHostPort>
    ///////////////////////////////////////////////////////////
	type record of NamedHostPort Address_List;
	
	///////////////////////////////////////////////////////////
    //  Type: NamedHostPort
    //
    //  Purpose:
    //    Descriptor for a transport address
    //
    //  Elements:
    //    id  - *charstring* - internal id for the transport address
    //    hostName - *charstring* - host name, or IP address
    //    portNumber - *integer* - port number for the transport protocol
    //
    ///////////////////////////////////////////////////////////
	type record NamedHostPort
	{
	  charstring id,
	  charstring hostName,
	  integer portNumber
	}	
	
	type boolean ReturnBoolean
	with { extension "done"};
	
	type record of charstring ro_charstring;
	
	type component IFW_BASE_CT 
	{
	  var IFW_Logging_Context v_IFW_logging := c_IFW_Logging_Context_init;
	};
	
	type record IFW_Logging_Context
	{
	  IFW_Logging_Params error_,
 	  IFW_Logging_Params warning,
 	  IFW_Logging_Params debug,
 	  IFW_Logging_Params debugv
	}
	
	const IFW_Logging_Context c_IFW_Logging_Context_init :=
	{
	  error_ := { 0, ACTION },
	  warning := { 0, ACTION },
	  debug := { 0, LOG },
	  debugv := { 0, LOG }
	}
	
	type record IFW_Logging_Params
	{
	  integer counter,
	  IFW_Logging_Filter filter
	}
	
	type enumerated IFW_Logging_Filter
	{
	  ACTION, LOG, SILENT
	}
	
	function f_isRunningGuard(in IFW_BASE_CT p_comp, in float p_timeout := 10.0)
	{
		timer T_wait;
	    if(p_comp.running)
    	{
      		T_wait.start(p_timeout);
		    log(%definitionId
        		& ": waiting for function to stop executing on component "
        		& log2str(p_comp) & ", timeout in "
        		& log2str(p_timeout) & " seconds.");
		    alt
		    {
		        [] p_comp.done
		        {
        		  log(%definitionId
		            & ": component " & log2str(p_comp)
		            & " returned done properly after waiting for "
		            & log2str(T_wait.read) & " seconds.");
        		}
		        [] T_wait.timeout
		        {
		          log(%definitionId
		            & ": Stopping function running on component " & log2str(p_comp)
		            & " due to timeout.");
		          p_comp.stop;
		        }
		        [else]
		        {
		          // busy loop to ensure no default altsteps are executed and
		          // unexpectedly ends our alt-statement by not calling 'repeat'
		          // correctly.
		          // v_dummy exists only to avoid busy-loop warning during compilation
		          var integer v_dummy;
		          repeat;
		        }
		    }
	    }
	}
	
	function f_checkResult(in Result pl_result)
	{
		if (ispresent(pl_result.errorCode))
		{
		  if (pl_result.errorCode != IPL4_ERROR_TEMPORARILY_UNAVAILABLE)
		  {
			log("Error: ", pl_result.errorCode, pl_result.os_error_text);
			setverdict(fail);
			stop;
		  }
		}
	}	
		
	function f_lookupAddress(charstring p_id, out NamedHostPort v_addr)
	return boolean
	{
	  
	  for (var integer i:=0; i<sizeof(tsp_addresses); i:=i+1)
	  {
	    if (tsp_addresses[i].id == p_id)
	    {
	      v_addr := tsp_addresses[i];
	      return true;
	    }
	  }
	  return false;
	}
	
	function f_IFW_wait(in float p_timeout)
	{
	  timer t:=p_timeout;
	  t.start;
	  t.timeout;
	}
	
	function f_IFW_log_error(in charstring p_log)
	runs on IFW_BASE_CT
	{
	  v_IFW_logging.error_.counter := v_IFW_logging.error_.counter + 1;
	  var charstring v_toLog := "[ERROR]["&int2str(v_IFW_logging.error_.counter)&"]: "&p_log
	  if (v_IFW_logging.error_.filter == ACTION) {
	    action(v_toLog);
	  } 
	  else if (v_IFW_logging.error_.filter == LOG) {
	    action(v_toLog);
	  }
	}
	
	function f_IFW_log_warning(in charstring p_log)
	runs on IFW_BASE_CT
	{
	  v_IFW_logging.warning.counter := v_IFW_logging.warning.counter + 1;
	  var charstring v_toLog := "[WARNING]["&int2str(v_IFW_logging.warning.counter)&"]: "&p_log
	  if (v_IFW_logging.warning.filter == ACTION) {
	    action(v_toLog);
	  } 
	  else if (v_IFW_logging.warning.filter == LOG) {
	    action(v_toLog);
	  }
	}
	
	function f_IFW_log_debug(in charstring p_log)
	runs on IFW_BASE_CT
	{
	  v_IFW_logging.debug.counter := v_IFW_logging.debug.counter + 1;
	  var charstring v_toLog := "[DEBUG]: "&p_log
	  if (v_IFW_logging.debug.filter == ACTION) {
	    action(v_toLog);
	  } 
	  else if (v_IFW_logging.debug.filter == LOG) {
	    action(v_toLog);
	  }
	}
	
	function f_IFW_log_verbose(in charstring p_log)
	runs on IFW_BASE_CT
	{
	  v_IFW_logging.debugv.counter := v_IFW_logging.debugv.counter + 1;
	  var charstring v_toLog := "[DEBUGV]: "&p_log
	  if (v_IFW_logging.debugv.filter == ACTION) {
	    action(v_toLog);
	  } 
	  else if (v_IFW_logging.debugv.filter == LOG) {
	    action(v_toLog);
	  }
	}

        type charstring ReturnCharstring
        with { extension "done" }

        type octetstring ReturnOctetstring
        with { extension "done" }


}
